<?php

require_once("Classes/Elephant.php");
require_once("Classes/Monkey.php");
require_once("Classes/Giraffe.php");
$num_animals = 5;
session_start();
if(!isset($_SESSION['animals'])) {
    initiateApp();
}

if(!empty($_POST) && isset($_POST['action'])) {
    switch($_POST['action']) {
        case "passTime": {
            passTime();
            break;
        }
        case "feedAnimals": {
            feedAnimals();
            break;
        }
        default:
            break;
    }
}

function initiateApp() {
    global $num_animals;
    $animals = ['monkeys' => [], 'elephants' => [], 'giraffes' => []];
    for($i = 0; $i < $num_animals; $i++) {
        $animals['monkeys'][] = new Monkey;
        $animals['elephants'][] = new Elephant;
        $animals['giraffes'][] = new Giraffe;
    }
    $_SESSION['animals'] = $animals;
    session_write_close();
}

function passTime() {
    global $num_animals;
    for($i = 0; $i < $num_animals; $i++) {
        $_SESSION['animals']['monkeys'][$i]->decreaseHealth();
        $_SESSION['animals']['elephants'][$i]->decreaseHealth();
        $_SESSION['animals']['giraffes'][$i]->decreaseHealth();
    }
    session_write_close();
}

function feedAnimals() {
    global $num_animals;
    $elephant_add_health = mt_rand(10, 15);
    $monkey_add_health = mt_rand(10, 15);
    $giraffe_add_health = mt_rand(10, 15);
    for($i = 0; $i < $num_animals; $i++) {
        $_SESSION['animals']['monkeys'][$i]->eat($monkey_add_health);
        $_SESSION['animals']['elephants'][$i]->eat($elephant_add_health);
        $_SESSION['animals']['giraffes'][$i]->eat($giraffe_add_health);
    }
    session_write_close();
}

?>

<table border="1">
    <thead>
        <th>Monkeys</th>
        <th>Elephants</th>
        <th>Giraffes</th>
    </thead>
    <tbody>
        <?php for($i = 0; $i < $num_animals; $i++): ?>
            <tr>
                <td>
                    <strong>HP:</strong> <?php echo $_SESSION['animals']['monkeys'][$i]->getHealth(); ?> <br>
                    <strong>Status:</strong> <?php echo $_SESSION['animals']['monkeys'][$i]->getStatus(); ?>
                </td>
                <td>
                    <strong>HP:</strong> <?php echo $_SESSION['animals']['elephants'][$i]->getHealth(); ?> <br>
                    <strong>Status:</strong> <?php echo $_SESSION['animals']['elephants'][$i]->getStatus(); ?>
                </td>
                <td>
                    <strong>HP:</strong> <?php echo $_SESSION['animals']['giraffes'][$i]->getHealth(); ?> <br>
                    <strong>Status:</strong> <?php echo $_SESSION['animals']['giraffes'][$i]->getStatus(); ?>
                </td>
            </tr>
        <?php endfor; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3" style="text-align:center">
                <form action="" method="post" style="display:inline-block">
                    <input type="hidden" name="action" value="passTime" />
                    <button type="submit">Pass Time</button>
                </form>
                <form action="" method="post" style="display:inline-block">
                    <input type="hidden" name="action" value="feedAnimals" />
                    <button type="submit">Feed Animals</button>
                </form>
            </td>
        </tr>
    </tfoot>
</table>
<?php
require_once("Animal.php");
class Monkey extends Animal
{
    public function decreaseHealth()
    {
        if($this->status === self::STATUS_DEAD) {
            return;
        }
        $this->health -= mt_rand(0, 20);
        if($this->health < 30) {
            $this->status = self::STATUS_DEAD;
        }
    }
}
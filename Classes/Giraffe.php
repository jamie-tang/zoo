<?php
require_once("Animal.php");
class Giraffe extends Animal
{
    public function decreaseHealth()
    {
        if($this->status === self::STATUS_DEAD) {
            return;
        }
        $this->health -= mt_rand(0, 20);
        if($this->health < 50) {
            $this->status = self::STATUS_DEAD;
        }
    }
}
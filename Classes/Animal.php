<?php

abstract class Animal
{
    const STATUS_HEALTHY = "healthy";
    const STATUS_DEAD = "dead";
    const STATUS_INJURED = "injured";

    protected $health;
    protected $status;

    /**
     * @return mixed
     */
    abstract protected function decreaseHealth();

    /**
     * Animal constructor.
     */
    public function __construct()
    {
        $this->health = 100.0;
        $this->status = self::STATUS_HEALTHY;
    }

    /**
     * @return float
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $health
     */
    public function eat($health)
    {
        if($this->status === self::STATUS_DEAD) {
            return;
        }
        $this->health = min(100.0, $this->health + $health);
    }
}
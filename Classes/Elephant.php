<?php
require_once("Animal.php");
class Elephant extends Animal
{
    public function decreaseHealth()
    {
        if($this->status === self::STATUS_DEAD) {
            return;
        }
        if($this->status === self::STATUS_INJURED) {
            $this->status = self::STATUS_DEAD;
            return;
        }
        $this->health -= mt_rand(0, 20);
        if($this->health < 70 && $this->status == self::STATUS_HEALTHY) {
            $this->status = self::STATUS_INJURED;
        }
    }

    public function eat($health)
    {
        if($this->status === self::STATUS_DEAD) {
            return;
        }
        $this->health = min(100.0, $this->health + $health);
        if($this->health > 70 && $this->status === self::STATUS_INJURED) {
            $this->status = self::STATUS_HEALTHY;
        }
        else if($this->health <= 70 && $this->status === self::STATUS_INJURED) {
            $this->status = self::STATUS_DEAD;
        }
    }
}